 package com.neu.realestates;
import org.hibernate.Session;
import org.hibernate.Transaction;
import com.neu.realestates.dao.DAO;
import com.neu.realestates.model.AllocatedHomes;
import com.neu.realestates.model.Building;
import com.neu.realestates.model.Role;
import com.neu.realestates.model.User;

public class MainTesting {
	
	public static void main(String args[])
	{
		
	    Session session = DAO.getSession();
		Transaction t = session.beginTransaction();
		
		Role r1 = new Role();		
		r1.setRoleName("System Admin");
		
		Role r2 = new Role();		
		r2.setRoleName("Lessor");
		
		Role r3 = new Role();		
		r3.setRoleName("Lesee");
		
		User user = new User();
		user.setUserName("sysadmin");
		user.setPassword("sysadmin");
		user.setStatus("Active");
		
		session.save(r1);
		session.save(r2);
		session.save(r3);
		
		user.setRole(r1);
		session.save(user);

		Building building = new Building();
		building.setPropertyName("CityView");
		building.setPropertyType("Building");
		building.setDateOfPurchase("04/05/1992");
		session.save(building);
		
		AllocatedHomes ah = new AllocatedHomes();
		ah.setBuilding(building);
		session.save(ah);
		
		building.getAllocatedHomes().add(ah);
		session.save(building);
		
		t.commit();
		session.close();
		System.out.println("Success");
		
	}

}
