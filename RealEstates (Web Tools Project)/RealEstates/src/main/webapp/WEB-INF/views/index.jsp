<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="false"%>

<!DOCTYPE html>
<html lang="en">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<link href="http://fonts.googleapis.com/css?family=Montserrat"
	rel="stylesheet" type="text/css">
<link href="http://fonts.googleapis.com/css?family=Lato"
	rel="stylesheet" type="text/css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script
	src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<style>

.jumbotron-banner {
	background-color: #1abc9c;
	padding: 100px 100px;
	color: #ffffff;
}

.bg-grey {
	background-color: #f6f6f6;
}

.container-fluid {
	padding: 60px 50px;
}

}
.logo {
	font-size: 500px;
}

body {
	font: 20px Montserrat, sans-serif;
	line-height: 1.8;
}

.bg-2 {
	padding: 100px 25px;
	 background-color: #474e5d; /* Dark Blue */
      color: #ffffff;
}

.container-fluid {
	padding-top: 70px;
	padding-bottom: 70px;
}

</style>

<script src="http://maps.googleapis.com/maps/api/js"></script>
<script>
var myCenter = new google.maps.LatLng(42.3601,-71.0589);

function initialize() {
var mapProp = {
  center:myCenter,
  zoom:12,
  scrollwheel:false,
  draggable:false,
  mapTypeId:google.maps.MapTypeId.ROADMAP
  };

var map = new google.maps.Map(document.getElementById("googleMap"),mapProp);

var marker = new google.maps.Marker({
  position:myCenter,
  });

marker.setMap(map);
}

google.maps.event.addDomListener(window, 'load', initialize);
</script>

<title>Real Estate</title>
</head>

<body>
<div class="jumbotron-banner text-center">

		<div class="container">
			<h1>Real Estate</h1>
			<p>We specialize in REALizing your dreams</p>
			

			<button class="btn btn-info btn-lg" data-toggle="modal"
				data-target="#myModal">Login</button>
		</div>
	</div>

	<div class="container-fluid bg-2 text-center">
		<div class="row">
		To view Buildings and Apartments <br>
		<button class="btn btn-default btn-lg" data-toggle="modal"
			data-target="#sign_up_Modal"> Create an account </button>
	</div>
	</div>

	<div class="container">

		<!-- Trigger the modal with a button -->

		<!-- Modal -->
		<div class="modal fade" id="myModal" role="dialog">
			<div class="modal-dialog">


				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Please enter your login credentials</h4>
					</div>

					<div class="modal-body">
						<form:form method="POST" commandName="user"
							class="form-horizontal" role="form">
							<div class="form-group">
								<label for="userName" class="control-lable col-sm-2">
									User Name:</label>

								<div class="col-sm-10">
									<form:input path="userName" class="form-control" id="userName"
										required="true" placeholder="Enter User Name"></form:input>
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-sm-2" for="pwd">Password:</label>
								<div class="col-sm-10">
									<form:password path="password" class="form-control"
										id="password" required="true" placeholder="Enter Password"></form:password>
								</div>
							</div>

							<div class="form-group">
								<div class="col-sm-offset-2 col-sm-10">
									<div class="checkbox">
										<label><input type="checkbox"> Remember me</label>
									</div>
								</div>
							</div>

							<div class="form-group">
								<div class="col-sm-offset-2 col-sm-10">
									<button type="submit" class="btn btn-default">Submit</button>
								</div>
							</div>
						</form:form>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="container">
	
	<!-- Modal -->
	<div class="modal fade" id="sign_up_Modal" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Please sign up to access our site</h4>
				</div>

				<div class="modal-body">

					<form:form method="POST" commandName="user" action="saveUser"
						modelAttribute="user" id="userForm">

						<div class="form-group">
							<label for="userName">First Name:</label>
							<form:input path="firstName" class="form-control" id="firstName"
								required="true" placeholder="Enter First Name"></form:input>
						</div>

						<div class="form-group">
							<label for="lastName">Last Name:</label>
							<form:input path="lastName" class="form-control" id="lastName"
								required="true" placeholder="Enter Last Name"></form:input>
						</div>

						<div class="form-group">
							<label for="userName">User Name:</label>
							<form:input path="userName" class="form-control" id="userName"
								required="true" placeholder="Enter User Name"></form:input>
							<div id="available" style="display: none; color: green;">
								<I><h6>Available</h6></I>
							</div>
							<div id="notavailable" style="display: none; color: red;">

								<h6>
									<i>User name not available. Suggested user name: '<span
										id="suggestion"></span>'
									</i>
								</h6>
							</div>
						</div>

						<div class="form-group">
							<label for="password">Password:</label>
							<form:password path="password" class="form-control" id="password"
								required="true" placeholder="Enter Password"></form:password>
						</div>

						<div class="form-group">
							<label for="email">Email:</label>

							<form:input path="emailId" class="form-control" id="email"
								required="true" placeholder="Enter Email"></form:input>
						</div>

						<form:hidden path="role" id="role" value="3" />
				</div>


				<form:button class="btn btn-primary ">Submit</form:button>
				</form:form>
			</div>
		</div>
	</div>
	</div>

	<div class="container">
	
	<div id="googleMap" style="height:400px;width:100%;"></div>
</div>
</body>