<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script
	src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<script
	src="http://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>

<style>
.jumbotron {
	background-color: #1abc9c;
	color: #fff;
	padding: 100px 25px;
}
</style>

<title>Insert title here</title>

</head>

<body>

	<div class="jumbotron text-center">
		<div class="container">
		<a href="logout"> Logout <span class="glyphicon glyphicon-off"></span>
			</a>
			<a href="home"><h1> Real Estate</h1></a>
			<p>We specialize in REALizing your dreams</p>



			<c:if test="${sessionScope.userRole == 1}">

				<a href="addUser">
					<button class="btn btn-default btn-lg">Add Lessor</button>
				</a>

			</c:if>

		</div>
	</div>


	<div>


		<%--For Lessor --%>

		<c:if test="${sessionScope.userRole == 2}">

			<h2>
				<a href="addBuilding"> Add New Building </a>
			</h2>
			<h2>
				<a href="addHome"> Add New Apartment </a>
			</h2>
		</c:if>



		<form:form method="POST" commandName="user" action="saveUser"
			modelAttribute="user" id="createUserForm">

			<div class="container">
				<div class="row">
					<div class="col-sm-3">
						<div class="form-group">
							<label for="userName">First Name:</label>
							<form:input path="firstName" class="form-control" id="firstName"
								required="true" placeholder="Enter First Name"></form:input>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-sm-3">
						<div class="form-group">
							<label for="lastName">Last Name:</label>
							<form:input path="lastName" class="form-control" id="lastName"
								required="true" placeholder="Enter Last Name"></form:input>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-sm-3">
						<div class="form-group">
							<label for="userName">User Name:</label>
							<form:input path="userName" class="form-control" id="userName"
								required="true" placeholder="Enter User Name"></form:input>
							<div id="available" style="display: none; color: green;">
								<I><h6>Available</h6></I>
							</div>
							<div id="notavailable" style="display: none; color: red;">
								<h6>
									<i>User name not available. Suggested user name: '<span
										id="suggestion"></span>'
									</i>
								</h6>
							</div>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-sm-3">
						<div class="form-group">
							<label for="password">Password:</label>
							<form:password path="password" class="form-control" id="password"
								required="true" placeholder="Enter Password"></form:password>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-sm-3">
						<div class="form-group">
							<label for="email">Email:</label>

							<form:input path="emailId" class="form-control" id="email"
								required="true" placeholder="Enter Email"></form:input>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-sm-3">
						<div class="form-group">
							<label for="password">Role:</label>

							<form:select path="role" id="role" items="${roleList}"
								class="form-control" itemValue="roleId" itemLabel="roleName">
							</form:select>
						</div>
					</div>
				</div>

				<form:button class="btn btn-primary ">Submit</form:button>

			</div>
		</form:form>

		<script>
		$("#userName")
				.change(
						function() {
							var q = $('#userName').val();

							$
									.ajax({
										url : 'checkifUserIdisAvailable',
										type : "POST",
										data : {
											'id' : q
										},
										success : function(data) {
											var result = data;
											if (result == 0) {

												document
														.getElementById('available').style.display = 'block';
												document
														.getElementById('notavailable').style.display = 'none';

											} else {
												document
														.getElementById('available').style.display = 'none';
												document
														.getElementById('notavailable').style.display = 'block';

												document
														.getElementById('suggestion').innerHTML = q
														+ result;
											}
										},
										error : function() {
											alert('Currently unable to perform username check');
										}

									});
							event.preventDefault();
						});

		$(document).ready(
				function() {

					$('#createUserForm').validate(
							{
								rules : {
									email : {
										required : true,
										email : true,
										
									}
								},
								highlight : function(element) {
									$(element).closest('.control-group')
											.removeClass('success').addClass(
													'error');
								},
								success : function(element) {
									element.text('OK!').addClass('valid')
											.closest('.control-group')
											.removeClass('error').addClass(
													'success');
								}
							});
				});
	</script>
</body>
</html>