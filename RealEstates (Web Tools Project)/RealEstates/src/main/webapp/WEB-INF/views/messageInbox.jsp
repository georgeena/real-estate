<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>


<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>


<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script
	src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<link href="http://fonts.googleapis.com/css?family=Montserrat"
	rel="stylesheet">

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<style>
.jumbotron {
	background-color: #1abc9c;
	color: #fff;
	padding: 100px 25px;
}
</style>

<title>Messages</title>
</head>
<body>


	<div class="jumbotron text-center">
		<div class="container">
		<a href="logout"> Logout <span class="glyphicon glyphicon-off"></span>
			</a>
			<a href="home"><h1> Real Estate</h1></a>
			<p>We specialize in REALizing your dreams</p>
		</div>
	</div>

	<!--  FOR LESSEE -->

	<c:if test="${sessionScope.userRole ==3 }">
		<div class="form-group">
			<form:form method="GET" action="getMessages"
				commandName="getMessages">

				<c:if test="${not empty requestScope.sentList}">
					<table class="table table-hover">
						<thead>
							<tr>
								<th>Message ID</th>
								<th>Message Date</th>
								<th>Message</th>
								<th>Sender</th>

							</tr>
						</thead>
						<tbody>
							<c:forEach var="message" items="${requestScope.sentList}">
								<tr>
									<td>${message.messageId}</td>
									<td>${message.timeStamp}</td>
									<td>${message.message }</td>
									<td>${message.sender.userName}</td>
							</c:forEach>
						</tbody>
					</table>
				</c:if>
				<c:if test="${empty requestScope.sentList}">
				Unable to retrieve list
			</c:if>
			</form:form>
		</div>
	</c:if>

	<!--  To keep a track of sent messages -->
	<!--  FOR LESSOR  -->
	<c:if test="${sessionScope.userRole ==2 }">


		<div class="form-group">
			<form:form method="GET" action="getMessages"
				commandName="getMessages">

				<c:if test="${not empty requestScope.receivedList}">
					<table class="table table-hover">
						<thead>
							<tr>
								<th>Message ID</th>
								<th>Message Date</th>
								<th>Message</th>
								<th>Sender</th>

							</tr>
						</thead>
						<tbody>
							<c:forEach var="message" items="${requestScope.receivedList}">
								<tr>
									<td>${message.messageId}</td>
									<td>${message.timeStamp}</td>
									<td>${message.message }</td>
									<td>${message.reciever.userName}</td>

							</c:forEach>
						</tbody>
					</table>
				</c:if>
				<c:if test="${empty requestScope.sentList}">
				Unable to retrieve list
			</c:if>

			</form:form>
	</c:if>
	</div>






</body>
</html>