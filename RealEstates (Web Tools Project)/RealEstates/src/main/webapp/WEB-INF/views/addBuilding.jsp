<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script
	src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<script
	src="http://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css"></script>

<script
	src="http://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
<script
	src="http://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>

<title>Add Buildings</title>
<script>
	$(function() {
		$("#datepicker").datepicker({
			autoclose : true,
			todayHighlight : true
		}).datepicker('update', new Date());
		;
	});
</script>
<style>
.jumbotron {
	background-color: #1abc9c;
	color: #fff;
	padding: 25px 25px;
}

.container-fluid {
	padding-top: 25px;
	padding-bottom: 50px;
}
</style>

</head>
<body>

	<div class="jumbotron text-center">
		<div class="container-fluid center">
			<a href="logout"> Logout <span class="glyphicon glyphicon-off"></span>
			</a> <a href="home"><h1>Real Estate</h1></a>
			<p>We specialize in REALizing your dreams</p>


			<c:if test="${sessionScope.userRole == 2}">
				<a href="addBuilding">
					<button class="btn btn-default btn-lg">Add a Building</button>
				</a>
				<a href="addHome">
					<button class="btn btn-default btn-lg">Add an Apartment</button>
				</a>
			</c:if>
		</div>
	</div>

	<div>

		<c:if test="${sessionScope.userRole == 1}">
			<li class="dropdown"><a class="dropdown-toggle"
				data-toggle="dropdown" href="#">Users<span class="caret"></span></a>
				<ul class="dropdown-menu">
					<li><a href="addUser">Add User</a></li>

				</ul></li>
		</c:if>

		<h3 class="text">Add Building</h3>
		<form:form method="POST" commandName="Building" action="saveBuilding"
			modelAttribute="building" id="buildingForm">

			<div class="container">
				<div class="row">
					<div class="col-sm-3">
						<div class="form-group">
							<label for="propertyType">Property Type:</label>

							<form:select path="propertyType" id="propertyType"
								class="form-control">
								<form:option value="Building"> Building </form:option>

							</form:select>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-sm-3">
						<div class="form-group">
							<label for="propertyName"> Name:</label>
							<form:input path="propertyName" class="form-control"
								id="propertyName" required="true"
								placeholder="Enter Building Name"></form:input>
						</div>
					</div>
				</div>


				<div class="row">
					<div class="col-sm-3">
						<div class="form-group">
							<label for="dateOfPurchase">Date of Entry:</label>

							<div id="datepicker" class="input-group date"
								data-date-format="mm-dd-yyyy">
								<form:input path="dateOfPurchase" class="form-control"
									id="dateOfPurchase" required="true"
									placeholder="Enter Date of Entry" readonly="true"></form:input>
								<span class="input-group-addon"><i
									class="glyphicon
										glyphicon-calendar"></i></span>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="container">
				<c:if test="${requestScope.message == 'success'}">
					<div class="alert alert-success" role="alert">New Building
						added Successfully</div>
				</c:if>
				<c:if test="${requestScope.message == 'failure'}">
					<div class="alert alert-danger" role="alert">Unable to add
						new Building</div>
				</c:if>

				<form:button class="btn btn-primary ">Submit</form:button>
		</form:form>
	</div>
</body>
</html>