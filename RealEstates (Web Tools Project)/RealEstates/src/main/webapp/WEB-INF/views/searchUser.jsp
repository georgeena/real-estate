<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script
	src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<link href="http://fonts.googleapis.com/css?family=Montserrat"
	rel="stylesheet">

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<title>Search Apartments</title>

<style>
.jumbotron {
	background-color: #1abc9c;
	color: #fff;
	padding: 50px 25px;
}
</style>

</head>
<body>
	<div class="jumbotron text-center">
		<div class="container">
		<a href="logout"> Logout <span class="glyphicon glyphicon-off"></span>
			</a>
			<a href="home"><h1> Real Estate</h1></a>
			<p>We specialize in REALizing your dreams</p>
		</div>
	</div>

<div class="container">
<div class="row">
<div class="col-sm-3">
	<div class="form-group">
		<form:form method="POST" action="searchforLesee">
			<div id="custom-search-input">
				<div class="input-group col-md-12">
					<input type="hidden" id="homeId" name="homeId" value="${requestScope.homeId}" />
					<input type="text" id="userName" name="userName"
						class="search-query form-control"
						placeholder="Search by User Name" /> <span
						class="input-group-btn">
						<button class="btn btn-default" type="button">
							<span class=" glyphicon glyphicon-search"></span>
						</button>
					</span>
				</div>
			</div>

		</form:form>
	</div>
</div>
</div>
</div>
	<c:if test="${not empty requestScope.usersList}">
		<div class="form-group">
			<form:form method="POST">
				<h4 class="text">User Details</h4>
				<table class="table table-hover">
					<thead>
						<tr>
							<th>First Name</th>
							<th>Last Name</th>
							<th>Email</th>

						</tr>
					</thead>
					<tbody>
						<c:forEach var="user" items="${requestScope.usersList}">
							<tr>
								<td>${user.firstName}</td>
								<td>${user.lastName}</td>
								<td>${user.emailId}</td>
								<td><a class="btn btn-primary"
									href="saveAllocation?userId=${user.userId}&homeId=${requestScope.homeId}">Save
										Allocation</a></td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</form:form>
		</div>
	</c:if>
</body>
</html>