
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script
	src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<title>Real Estate</title>
<style>
.jumbotron {
	background-color: #1abc9c;
	color: #fff;
	padding: 25px 25px;
}
</style>

</head>

<body>

	<div class="jumbotron text-center">
		<div class="container">
		<a href="logout"> Logout <span class="glyphicon glyphicon-off"></span>
			</a>
			<a href="home"><h1> Real Estate</h1></a>
			<p>We specialize in REALizing your dreams</p>

			<c:if test="${sessionScope.userRole == 1}">
				<h2>
					<a href="addUser">
						<button class="btn btn-default btn-lg">Add Lessor</button>
					</a>
				</h2>
			</c:if>

			<c:if test="${sessionScope.userRole == 2}">
				<h2>
					<a href="addBuilding">
						<button class="btn btn-default btn-lg">Add a new
							Building/Apartment</button>
					</a>

				</h2>
				<h2>
					<a href="getMessages"><button class="btn btn-default btn-lg">
							MailBox</button> </a>

				</h2>
			</c:if>


			<c:if test="${sessionScope.userRole ==3 }">
				<h2>
					<a href="searchBuilding">
						<button class="btn btn-default btn-lg">Search for your
							dream home</button>
					</a>
				</h2>
				<h2>
					<a href="getMessages"><button class="btn btn-default btn-lg"> MailBox</button> </a>
				</h2>
			</c:if>
		</div>
	</div>

	

	<div class="col-md-6">
			
		<h3>
			<I> Unable to perform requested action. Please contact system administrator !!</I>
		</h3>		
	</div>
</body>