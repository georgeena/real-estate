<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script
	src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<link href="http://fonts.googleapis.com/css?family=Montserrat"
	rel="stylesheet">

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<title>Search Apartments</title>

<style>
.jumbotron {
	background-color: #1abc9c;
	color: #fff;
	padding: 100px 25px;
}
</style>

</head>
<body>
	<div class="jumbotron text-center">
		<div class="container">
		<a href="logout"> Logout <span class="glyphicon glyphicon-off"></span>
			</a>
			<a href="home"><h1> Real Estate</h1></a>
			<p>We specialize in REALizing your dreams</p>
		</div>
	</div>

	<!-- 
	<c:if test="${sessionScope.userRole == 3}">
		
				<li><a href="searchBuilding"> Search Buildings </a></li>
				
	</c:if>
	
	 -->

	<div class="form-group">
		<form:form method="POST" action="searchApartment"
			commandName="searchApartment">
			<c:if test="${not empty requestScope.aptList}">
				<table class="table table-hover">
					<thead>
						<tr>
							<th>Home ID</th>
							<th>No of Rooms</th>
							<th>No of Bathrooms</th>
							<th>Rent</th>

						</tr>
					</thead>
					<tbody>
						<c:forEach var="home" items="${requestScope.aptList}">
							<tr>
								<td>${home.propertyId}</td>
								<td>${home.noOfRooms}</td>
								<td>${home.noOfBathrooms}</td>
								<td>${home.rent }</td>
								<!--  <td>${user.status}</td> -->
								<c:if test="${sessionScope.userRole == 3}">
									<td><a class="btn btn-primary"
										href="sendMessage?homeId=${home.propertyId}">Request for
											Viewing</a></td>
								</c:if>
								<c:if test="${sessionScope.userRole == 2}">
									<td><a class="btn btn-primary"
										href="searchUser?homeId=${home.propertyId}">Allocate</a></td>
								</c:if>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</c:if>
			<c:if test="${empty requestScope.aptList}">
				No vacant apartments 
			</c:if>
		</form:form>
	</div>

</body>
</html>