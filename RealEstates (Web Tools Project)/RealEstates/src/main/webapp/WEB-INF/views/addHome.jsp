<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<link href="http://fonts.googleapis.com/css?family=Montserrat"
	rel="stylesheet" type="text/css">
<link href="http://fonts.googleapis.com/css?family=Lato"
	rel="stylesheet" type="text/css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script
	src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<title>Add Home</title>
<style>
.jumbotron {
	background-color: #1abc9c;
	color: #fff;
	padding: 100px 25px;
}
</style>
</head>
<body>
	<div class="jumbotron text-center">
		<div class="container">
		<a href="logout"> Logout <span class="glyphicon glyphicon-off"></span>
			</a>
			<a href="home"><h1> Real Estate</h1></a>
			<p>We specialize in REALizing your dreams</p>
		</div>
	</div>





	<c:if test="${sessionScope.userRole == 1}">

		<a href="addUser">Add User</a>
		<!-- <li><a href="manageUsers">Manage Users</a></li>-->
	</c:if>

	<c:if test="${sessionScope.userRole == 2}">
		
	</c:if>

	<div class="container">
		<c:if test="${requestScope.message == 'success'}">
			<div class="alert alert-success" role="alert">New Apartment
				added Successfully</div>
		</c:if>
		<c:if test="${requestScope.message == 'failure'}">
			<div class="alert alert-danger" role="alert">Unable to add new
				apartment</div>
		</c:if>

		<h3 class="text">Add Apartment</h3>

		<form:form method="POST" commandName="home" action="saveHome"
			modelAttribute="home" id="apartment">
  
			<div class="container">
				<div class="row">
					<div class="col-sm-3">
						<div class="form-group">
							<label for="BuildingName"> Building: </label>
							<form:select path="building" id="building"
								items="${buildingList}" class="form-control"
								itemValue="propertyId" itemLabel="propertyName">
							</form:select>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-sm-3">
						<div class="form-group">
							<label for=noOfRooms> Number of Rooms </label>
							<form:input path="noOfRooms" class="form-control" id="noOfRooms"
								required="true" number="true"
								placeholder="Enter enter the no of rooms in the apartment"></form:input>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-sm-3">
						<div class="form-group">
							<label for=noOfBathrooms> Number of Bathrooms </label>
							<form:input path="noOfBathrooms" class="form-control"
								id="noOfRooms" required="true" number="true"
								placeholder="Enter enter the no of bathrooms in the apartment"></form:input>
						</div>
					</div>
				</div>
				
				
				<div class="row">
					<div class="col-sm-3">
						<div class="form-group">
							<label for=rent> Rent </label>
							<form:input path="rent" class="form-control"
								id="rent" required="true" number="true"
								placeholder="Enter enter rent of the apt"></form:input>
						</div>
					</div>
				</div>
				
				
				<form:hidden path="propertyType" value="Home" />
				<form:button class="btn btn-primary ">Submit</form:button>
			</div>
		</form:form>

	</div>
</body>
</html>