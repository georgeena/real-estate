
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script
	src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<title>Real Estate</title>
<style>
.jumbotron {
	background-color: #1abc9c;
	color: #fff;
	padding: 25px 25px;
}
</style>

</head>

<body>

	<div class="jumbotron text-center">
		<div class="container">
		<a href="logout"> Logout <span class="glyphicon glyphicon-off"></span>
			</a>
			<a href="home"><h1> Real Estate</h1></a>
			<p>We specialize in REALizing your dreams</p>

			<c:if test="${sessionScope.userRole == 1}">
				<h2>
					<a href="addUser">
						<button class="btn btn-default btn-lg">Add Lessor</button>
					</a>
				</h2>
			</c:if>

			<c:if test="${sessionScope.userRole == 2}">
				<h2>
					<a href="addBuilding">
						<button class="btn btn-default btn-lg">Add a new
							Building/Apartment</button>
					</a>

				</h2>
				<h2>
					<a href="getMessages"><button class="btn btn-default btn-lg">
							MailBox</button> </a>

				</h2>
			</c:if>


			<c:if test="${sessionScope.userRole ==3 }">
				<h2>
					<a href="searchBuilding">
						<button class="btn btn-default btn-lg">Search for your
							dream home</button>
					</a>
				</h2>
				<h2>
					<a href="getMessages"><button class="btn btn-default btn-lg"> MailBox</button> </a>
				</h2>
			</c:if>
		</div>
	</div>

	<!-- DASHBOARD -->

	<div class="col-md-6">
			<c:if test="${requestScope.message == 'success'}">
					<div class="alert alert-success" role="alert">Action performed Successfully</div>
				</c:if>
				<c:if test="${requestScope.message == 'failure'}">
					<div class="alert alert-danger" role="alert">Unable to perform the requested action</div>
				</c:if>
		<h3>
			<I> My Properties </I>
		</h3>

		<c:choose>
			<c:when test="${not empty requestScope.propertyList}">

				<form:form>
					<table class="table table-condensed">
						<thead>
							<tr>
								<th>Property ID</th>
								<th>Property Name</th>
								<th>Property Type</th>
							</tr>
						</thead>
						<tbody>

							<c:forEach var="building" items="${requestScope.propertyList}">
								<tr>
									<td>${building.propertyId}</td>
									<td>${building.propertyName}</td>
									<td>${building.propertyType}</td>
									<td><a class="btn btn-primary"
										href="searchApartment?buildingId=${building.propertyId}">Select</a></td>

								</tr>
							</c:forEach>
						</tbody>
					</table>
				</form:form>
			</c:when>
			<c:when test="${not empty requestScope.homeList}">
				<form:form>
					<table class="table table-condensed">
						<thead>
							<tr>
								<th>Home ID</th>
								<th>No of Rooms</th>
								<th>No of Bathrooms</th>
								<th> Rent </th>
							</tr>
						</thead>
						<tbody>

							<c:forEach var="home" items="${requestScope.homeList}">
								<tr>
									<td>${home.propertyId}</td>
									<td>${home.noOfRooms}</td>
									<td>${home.noOfBathrooms}</td>
									<td>${home.rent}</td>
															</tr>
							</c:forEach>
						</tbody>
					</table>
				</form:form>
			</c:when>
			<c:otherwise>
				<h6>
					<I>You don't have associated properties </I>
				</h6>
			</c:otherwise>
		</c:choose>
	</div>
</body>