package com.neu.realestates.dao;

import java.util.ArrayList;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import com.neu.realestates.model.AllocatedHomes;
import com.neu.realestates.model.User;

public class AllocatedHomesDAO extends DAO {

	public int allocatedHomesCount(User user) throws Exception {

		try {
			Session session = getSession();
			ArrayList<AllocatedHomes> software = (ArrayList<AllocatedHomes>) session
					.createCriteria(AllocatedHomes.class)
					.add(Restrictions.eq("installedBy", user))
					.list();
			return software.size();
		} catch (HibernateException e) {
			return 0;
		}
	}
	
}
