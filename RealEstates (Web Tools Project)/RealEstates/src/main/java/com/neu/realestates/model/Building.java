package com.neu.realestates.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

public class Building extends Property implements Serializable
{
	private User user;
	private Set<AllocatedHomes> allocatedHomes;
	private Set<Log> log;
	
	public Building()
	{
		allocatedHomes = new HashSet<AllocatedHomes>();
		log = new HashSet<Log>();
		
	}
	
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public Set<AllocatedHomes> getAllocatedHomes() {
		return allocatedHomes;
	}
	public void setAllocatedHomes(Set<AllocatedHomes> allocatedHomes) {
		this.allocatedHomes = allocatedHomes;
	}
	public Set<Log> getLog() {
		return log;
	}
	public void setLog(Set<Log> log) {
		this.log = log;
	}
	
	
	

}
