package com.neu.realestates.dao;

import java.util.ArrayList;
import java.util.Date;
//import org.springframework.transaction.annotation.Transactional;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.neu.realestates.model.AllocatedHomes;
import com.neu.realestates.model.Building;
import com.neu.realestates.model.Home;
import com.neu.realestates.model.Log;
import com.neu.realestates.model.Property;
import com.neu.realestates.model.Role;
import com.neu.realestates.model.User;
//@Transactional
//@Repository

public class PropertyDAO extends DAO {
	
	public Log updateLog(String string) {
		try {
			Session session = getSession();
			//session.beginTransaction();
			Log log = new Log();
			log.setLogDescription(string);
			log.setTimeStamp(new Date().toString());
			session.save(log);
			//session.getTransaction().commit();
			return log;
		} catch (HibernateException e) {
			return null;
		}
	}
	
	
	public int addBuilding(Building building) throws Exception {
		try {
			Session session = getSession();
			session.beginTransaction();
			building.getLog().add(updateLog("Building has been created"));
			session.save(building);
			session.getTransaction().commit();
			return 1;
		} catch (HibernateException e) {
			return 0;
		}
	}

	// Home Begins
	public int addHome(Home home) throws Exception {
		try {
			Session session = getSession();
			session.beginTransaction();
			session.save(home);
			session.getTransaction().commit();
			return 1;
		} catch (HibernateException e) {
			return 0;
		}
	}

	// Finding Homes using ID
	public Home findHomeById (int id) {
		Session session = getSession();

		Home home = (Home) session.createCriteria(Home.class)
				//.add(Restrictions.isNull("user")) // doubtful about this
				.add(Restrictions.eq("propertyId", id)).uniqueResult();
		return home;

	}
	
	public void updateAllocation(int homeId, User lesee) throws Exception {
		try {
			try {
				Session session = getSession();
				session.beginTransaction();
				Home home = (Home) session.createCriteria(Home.class)
						.add(Restrictions.eq("propertyId", homeId))
						.uniqueResult();
				home.setLesee(lesee);
				session.update(home);
				session.getTransaction().commit();
			} catch (HibernateException e) {
				throw new Exception("Could not find user" + e);
			}
		} catch (HibernateException e) {
			System.out.println(e.toString());
		}
	}
	
	// Finding Building using ID
		public Building findBuildingById (String s) {
			
			Session session = getSession();

			Building building = (Building) session.createCriteria(Building.class)
					//.add(Restrictions.isNull("user")) // doubtful about this
					.add(Restrictions.eq("propertyId", Integer.parseInt(s))).uniqueResult();
			return building;
			
		}

	// Buildings owned by user
	public ArrayList<Building> userBuilding(User user) throws Exception {
		try {
			Session session = getSession();

			ArrayList<Building> buildings = (ArrayList<Building>) session.createCriteria(Building.class)
					.add(Restrictions.eq("user", user)).list();
			return buildings;
		} catch (HibernateException e) {
			throw new Exception("Could not find buildings" + e);
		}
	}
	
	public ArrayList<Home> userHomes(User user) throws Exception {
		try {
			Session session = getSession();			
			ArrayList<Home> home = (ArrayList<Home>) session.createCriteria(Home.class)
					.add(Restrictions.eq("lesee", user)).list();
			return home;
		} catch (HibernateException e) {
			throw new Exception("Could not find homes " + e);
		}
	}

		//search all Buildings
	public ArrayList<Building> searchAllBuildings(int propertyId) throws Exception {
		try {
			Session session = getSession();

			ArrayList<Building> building = (ArrayList<Building>) session
					.createCriteria(Building.class)
					.add(Restrictions.eq("propertyId", propertyId)).list();

			return building;
		} catch (HibernateException e) {
			throw new Exception("Could not find user" + e);
		}
	}
	
	//available Homes List
	public ArrayList<Home> availableHomeList(Building b) throws Exception {
		try {

			Session session = getSession();
			ArrayList<Home> homes = (ArrayList<Home>) session
					.createCriteria(Home.class)
					.add(Restrictions.isNull("lesee"))
					.add(Restrictions.eqOrIsNull("building", b))
					.add(Restrictions.ne("propertyType", "Building")).list();
			return homes;		
		
		} catch (HibernateException e) {
			throw new Exception("Could not find home" + e);
		}
	}
	

	
	//ask difference between this and arrayList one
	public Building searchBuildingbyId(int propertyId) throws Exception {
		try {
			Session session = getSession();

			Building building = (Building) session
					.createCriteria(Building.class)
					.add(Restrictions.eq("propertyId", propertyId)).uniqueResult();

			Hibernate.initialize(building.getAllocatedHomes());
			for (AllocatedHomes ah : building.getAllocatedHomes()) {
				Hibernate.initialize(ah.getHome());
			}

			return building;
		} catch (HibernateException e) {
			throw new Exception("Could not find user" + e);
		}
	}

	public Home searchHomebyId(int homeId) throws Exception{		
		try {
			Session session = getSession();

			Home home = (Home) session
					.createCriteria(Home.class)
					.add(Restrictions.eq("homeId", homeId)).uniqueResult();

			return home;
		} catch (HibernateException e) {
			throw new Exception("Could not find user" + e);
		}
	}


		
		public int noOfBuildings(User user) throws Exception {

			try {
				Session session = getSession();

				ArrayList<Building> building = (ArrayList<Building>) session
						.createCriteria(Building.class)
						.list();
				return building.size();
			} catch (HibernateException e) {
				return 0;
			}
		}
		
		public int buildingCount(User user) throws Exception {

			try {
				Session session = getSession();
				ArrayList<Building> building = (ArrayList<Building>) session
						.createCriteria(Building.class)
						.add(Restrictions.eq("user", user))
						.list();
				return building.size();
			} catch (HibernateException e) {
				return 0;
			}
		}
		
				
		
		public ArrayList<Building> retrieveBuildingList() throws Exception {
			try {
				Session session = getSession();
				ArrayList<Building> buildings = (ArrayList<Building>) session
						.createCriteria(Building.class)
						//.add(Restrictions.eq("user",user))
						.add(Restrictions.ne("propertyType", "Home")).list();
				return buildings;
			} catch (HibernateException e) {
				throw new Exception("Could not find any buildings" + e);
			}
		}
		

		public ArrayList<Building> retrieveMyBuildingList(User u) throws Exception
		{
			try {
				Session session = getSession();
				ArrayList<Building> buildings = (ArrayList<Building>) session
						.createCriteria(Building.class)
						.add(Restrictions.eq("user",u))
						.add(Restrictions.ne("propertyType", "Home")).list();
				return buildings;
			} catch (HibernateException e) {
				throw new Exception("Could not find any buildings" + e);
			}
		}
		
	}

	


	
	
	
	




