package com.neu.realestates.dao;

import java.util.ArrayList;
import java.util.Date;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import com.neu.realestates.model.Messages;
import com.neu.realestates.model.Role;
import com.neu.realestates.model.User;

public class UserDAO extends DAO {

	public UserDAO() {

	}

	public int addUser(User user) throws Exception {
		try {
			Session session = getSession();
			session.beginTransaction();
			session.save(user);
			session.getTransaction().commit();
			return 1;
		} catch (HibernateException e) {
			return 0;
		}

	}

	public int addMessage(Messages m) throws Exception {
		try {
			Session session = getSession();
			session.beginTransaction();
			m.setTimeStamp(new Date().toString());
			session.save(m);
			session.getTransaction().commit();
			return 1;
		} catch (HibernateException e) {
			return 0;
		}

	}

	public ArrayList<Messages> retrieveSentMessages(User u) throws Exception {
		try {
			Session session = getSession();

			ArrayList<Messages> sent = (ArrayList<Messages>) session.createCriteria(Messages.class)
					.add(Restrictions.eq("sender", u)).list();

			return sent;
		} catch (HibernateException e) {
			throw new Exception("Could not find user" + e);
		}
	}

	public ArrayList<Messages> retrieveInbox(User u) throws Exception {
		try {
			Session session = getSession();

			ArrayList<Messages> sent = (ArrayList<Messages>) session.createCriteria(Messages.class)
					.add(Restrictions.eq("reciever", u)).list();

			return sent;
		} catch (HibernateException e) {
			throw new Exception("Could not find user" + e);
		}
	}

	// search by id
	public User searchUserById(int userId) throws Exception {
		try {
			Session session = getSession();
			User user = (User) session.createCriteria(User.class).add(Restrictions.eq("userId", userId)).uniqueResult();
			return user;
		} catch (HibernateException e) {
			throw new Exception("Could not find user" + e);
		}
	}

	public int checkUserIDAvailability(String name) throws Exception {
		try {
			Session session = getSession();
			Query q = getSession().createQuery("from User where username = :username");
			q.setParameter("username", name);
			User user = (User) q.uniqueResult();

			if (user != null) {
				return suggestion(name);

			} else {
				return 0;
			}
		} catch (HibernateException e) {
			throw new Exception("Could not find user" + e);
		}

	}

	public int suggestion(String name) throws Exception {
		try {
			Session session = getSession();
			Query q = getSession().createQuery("from User where username = :username");
			int i = 1;
			User user = null;
			while (1 == 1) {
				String n = name + i;
				q.setParameter("username", n);
				user = (User) q.uniqueResult();
				if (user != null) {
					i++;
				} else {
					return i;
				}
			}

		} catch (HibernateException e) {
			throw new Exception("Could not find user" + e);
		}

	}

	public User queryUserByNameAndPassword(String name, String password) throws Exception {
		try {
			Session session = getSession();

			Query q = getSession().createQuery(
					"from User where username = :username and password = :password and status = 'Active' ");
			q.setParameter("username", name);
			q.setParameter("password", password);

			User user = (User) q.uniqueResult();

			return user;
		} catch (HibernateException e) {
			throw new Exception("Could not find user" + e);
		}
	}

	public int updateUserStatus(String username) throws Exception {
		try {
			Session session = getSession();

			User user = (User) session.createCriteria(User.class).add(Restrictions.eq("userName", username))
					.uniqueResult();

			if (user.getStatus().toString().equalsIgnoreCase("Active")) {
				user.setStatus("Inactive");
			} else {
				user.setStatus("Active");
			}
			session.beginTransaction();
			session.save(user);
			session.getTransaction().commit();
			return 1;
		} catch (HibernateException e) {
			return 0;
		}

	}

	public void updateLastLogin(String username) throws Exception {
		try {
			try {
				Session session = getSession();
				session.beginTransaction();
				User user = (User) session.createCriteria(User.class).add(Restrictions.like("userName", username))
						.uniqueResult();
				user.setLastLogin(new Date().toString());
				session.update(user);
				session.getTransaction().commit();
			} catch (HibernateException e) {
				throw new Exception("Could not find user" + e);
			}
		} catch (HibernateException e) {
			System.out.println(e.toString());
		}
	}

	public User searchByUserName(String username) throws Exception {
		try {
			Session session = getSession();

			User user = (User) session.createCriteria(User.class).add(Restrictions.like("userName", username))
					.uniqueResult();

			return user;
		} catch (HibernateException e) {
			throw new Exception("Could not find user" + e);
		}
	}

	public ArrayList<User> searchAllUsers(String userName, Role r) throws Exception {
		try {
			Session session = getSession();

			ArrayList<User> user = (ArrayList<User>) session.createCriteria(User.class).add(Restrictions.eq("role", r))
					.add(Restrictions.like("userName", userName, MatchMode.ANYWHERE)).list();

			return user;
		} catch (HibernateException e) {
			throw new Exception("Could not find user" + e);
		}
	}

}
