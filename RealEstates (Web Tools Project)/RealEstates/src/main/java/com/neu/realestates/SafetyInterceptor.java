package com.neu.realestates;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.neu.realestates.model.User;

public class SafetyInterceptor extends HandlerInterceptorAdapter {

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws IOException {

		if (request.getRequestURI().equalsIgnoreCase("/realestates/")) {
			return true;
		} else {
			HttpSession session = (HttpSession) request.getSession(true);
			if (session != null) {
				User user = (User) session.getAttribute("sessionUser");
				if (user != null) {
					// URL Security Check for Role 1
					if (user.getRole().getRoleId() == 1
							&& (request.getRequestURI().contains("home") || request.getRequestURI().contains("addUser")
									|| request.getRequestURI().contains("saveUser")
									|| request.getRequestURI().contains("logout")
									|| request.getRequestURI().contains("error")
									|| request.getRequestURI().contains("checkifUserIdisAvailable"))) {
						return true;
					} else if (user.getRole().getRoleId() == 2 && (request.getRequestURI().contains("home")
							|| request.getRequestURI().contains("addBuilding")
							|| request.getRequestURI().contains("saveBuilding")
							|| request.getRequestURI().contains("addHome")
							|| request.getRequestURI().contains("saveHome")
							|| request.getRequestURI().contains("searchBuilding")
							|| request.getRequestURI().contains("searchApartment")
							|| request.getRequestURI().contains("getMessages")
							|| request.getRequestURI().contains("logout")
							|| request.getRequestURI().contains("searchUser")
							|| request.getRequestURI().contains("searchforLesee")
							|| request.getRequestURI().contains("error")
							|| request.getRequestURI().contains("saveAllocation"))) {
						return true;
					} else if (user.getRole().getRoleId() == 3 && (request.getRequestURI().contains("home")
							|| request.getRequestURI().contains("searchBuilding")
							|| request.getRequestURI().contains("searchApartment")
							|| request.getRequestURI().contains("sendMessage")
							|| request.getRequestURI().contains("getMessages")
							|| request.getRequestURI().contains("logout")
							|| request.getRequestURI().contains("error")
							|| request.getRequestURI().contains("checkifUserIdisAvailable"))) {
						return true;
					} else {
						response.sendRedirect(request.getContextPath());
						return true;
					}
				} else {
					response.sendRedirect(request.getContextPath());
					return true;
				}
			} else {
				response.sendRedirect(request.getContextPath());
				return true;
			}
		}
	}
}
