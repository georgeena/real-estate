package com.neu.realestates.dao;

import java.util.ArrayList;

import org.hibernate.Query;

import com.neu.realestates.model.Role;

public class RoleDAO extends DAO 
{

	public ArrayList<Role> getUserRoles()
	{
		Query q = getSession().createQuery("from Role");
		ArrayList <Role> role = (ArrayList<Role>) q.list();
		return role;
	}
	
	public Role findRoleBy(int id)
	{
		Query q = getSession().createQuery("from Role where roleId = :roleId ");
		q.setParameter("roleId", id);
		Role role = (Role) q.uniqueResult();
		return role;
	}

}
