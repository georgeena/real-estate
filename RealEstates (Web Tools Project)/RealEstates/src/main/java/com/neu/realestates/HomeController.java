package com.neu.realestates;

import java.beans.PropertyEditorSupport;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.neu.realestates.dao.AllocatedHomesDAO;
import com.neu.realestates.dao.BuildingDAO;
import com.neu.realestates.dao.HomeDAO;
import com.neu.realestates.dao.PropertyDAO;
import com.neu.realestates.dao.RoleDAO;
import com.neu.realestates.dao.UserDAO;
import com.neu.realestates.model.Building;
import com.neu.realestates.model.Home;
import com.neu.realestates.model.Messages;
import com.neu.realestates.model.Role;
import com.neu.realestates.model.User;

/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {

	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);

	@Autowired
	private AllocatedHomesDAO allocatedHomesDAO;

	@Autowired
	private BuildingDAO buildingDAO;

	@Autowired
	private HomeDAO homeDAO;

	@Autowired
	private PropertyDAO propertyDAO;

	@Autowired
	private RoleDAO roleDAO;

	@Autowired
	private UserDAO userDAO;

	@InitBinder
	protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) throws Exception {
		binder.registerCustomEditor(Role.class, "role", new PropertyEditorSupport() {
			@Override
			public void setAsText(String text) {
				Role r = roleDAO.findRoleBy(Integer.parseInt(text));
				setValue(r);
			}
		});

		binder.registerCustomEditor(Home.class, "home", new PropertyEditorSupport() {
			@Override
			public void setAsText(String text) {
				Home h = propertyDAO.findHomeById(Integer.parseInt(text));
				setValue(h);
			}
		});
		binder.registerCustomEditor(Building.class, "building", new PropertyEditorSupport() {
			@Override
			public void setAsText(String text) {
				Building b = propertyDAO.findBuildingById((text));
				setValue(b);
			}
		});
	}

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String login(Locale locale, Model model, HttpServletRequest request) {
		User user = new User();
		Cookie[] cookies = request.getCookies();
		String userName = null;
		String userPassword = null;
		if (cookies != null && cookies.length >= 1) {
			for (Cookie c : cookies) {
				if (c.getName().equalsIgnoreCase("userName")) {
					userName = c.getValue();
				}
				if (c.getName().equalsIgnoreCase("userPassword")) {
					userPassword = c.getValue();
				}
			}
			try {
				User u = userDAO.queryUserByNameAndPassword(userName, userPassword);
				if (u != null) {
					HttpSession session = request.getSession();
					model.addAttribute("user", u);
					session.setAttribute("userName", u.getUserName());
					session.setAttribute("sessionUser", u);
					session.setAttribute("userRole", u.getRole().getRoleId());

					userDAO.updateLastLogin(u.getUserName());
					model.addAttribute("propertyList", propertyDAO.userBuilding(u));
					model.addAttribute("homeList", propertyDAO.userHomes(u));
					model.addAttribute("homeCount", allocatedHomesDAO.allocatedHomesCount(u));
					model.addAttribute("BuildingCount", propertyDAO.noOfBuildings(u));

					return "home";
				} else {
					model.addAttribute("user", user);
					return "index";
				}
			} catch (Exception e) {

			}
		}
		model.addAttribute("user", user);
		return "index";
	}

	@RequestMapping(value = "/", method = RequestMethod.POST)
	public String home(Locale locale, Model model, User user, HttpServletRequest request,
			HttpServletResponse response) {
		String remember = (request.getParameter("Remember") != null ? "Remember" : "No");

		try {

			HttpSession session = request.getSession();
			User u = userDAO.queryUserByNameAndPassword(user.getUserName(), user.getPassword());
			if (u != null) {
				model.addAttribute("user", u);
				session.setAttribute("userName", u.getUserName());
				session.setAttribute("sessionUser", u);
				if (remember != null && remember.equalsIgnoreCase("Remember")) {
					Cookie userName = new Cookie("userName", user.getUserName());
					userName.setMaxAge(600);
					response.addCookie(userName);
					Cookie userPassword = new Cookie("userPassword", user.getPassword());
					userPassword.setMaxAge(600);
					response.addCookie(userPassword);
				}
				session.setAttribute("userRole", u.getRole().getRoleId());
				userDAO.updateLastLogin(u.getUserName());
				model.addAttribute("propertyList", propertyDAO.userBuilding(u));
				model.addAttribute("homeList", propertyDAO.userHomes(u));
				model.addAttribute("homeCount", allocatedHomesDAO.allocatedHomesCount(u));
				model.addAttribute("BuildingCount", propertyDAO.noOfBuildings(u));

				return "home";
			} else {
				model.addAttribute("user", user);
				return "index";
			}
		} catch (Exception e) {

		}
		return "index";
	}

	@RequestMapping(value = "home")
	public String loginPage(Locale locale, Model model, User user, HttpServletRequest request) {
		try {
			// model.addAttribute("tipList", userTipsDAO.userTips());
			HttpSession session = request.getSession();
			User u = (User) session.getAttribute("sessionUser");
			model.addAttribute("propertyList", propertyDAO.userBuilding(u));
			model.addAttribute("homeList", propertyDAO.userHomes(u));
			model.addAttribute("homeCount", allocatedHomesDAO.allocatedHomesCount(u));
			model.addAttribute("buildingCount", propertyDAO.buildingCount(u));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "home";
	}

	// value = addUser is the part that picked up which page to display

	@RequestMapping(value = "addUser", method = RequestMethod.GET)
	public String addLessorsPage(Locale locale, Model model, @ModelAttribute("user") User user) {
		model.addAttribute("roleList", roleDAO.getUserRoles());
		model.addAttribute("user", new User());
		return "addUser";
	}

	// change this to User
	@RequestMapping(value = "saveUser", method = RequestMethod.POST)
	public String saveUser(Locale locale, Model model, User user, HttpServletRequest request) {
		try {
			user.setStatus("Active");
			int status = userDAO.addUser(user);
			model.addAttribute("roleList", roleDAO.getUserRoles());
			model.addAttribute("user", new User());
			
			if (status > 0) {
				HttpSession session = request.getSession();
				User u = (User) session.getAttribute("sessionUser");
				model.addAttribute("message", "success");
				model.addAttribute("propertyList", propertyDAO.userBuilding(u));
				model.addAttribute("homeList", propertyDAO.userHomes(u));
				model.addAttribute("homeCount", allocatedHomesDAO.allocatedHomesCount(u));
				model.addAttribute("BuildingCount", propertyDAO.noOfBuildings(u));
				return "home";

			} else {
				model.addAttribute("message", "failure");
				return "error";

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "error";
	}

	// Method rendering Add Building Page
	@RequestMapping(value = "addBuilding", method = RequestMethod.GET)
	public String addBuildingPage(Locale locale, Model model) {
		model.addAttribute("building", new Building());
		return "addBuilding";
	}

	// Method saving new Building to DB
	@RequestMapping(value = "saveBuilding", method = RequestMethod.POST)
	public String saveBuildingToDatabase(Locale locale, Model model, Building building, HttpServletRequest request) {
		try {

			HttpSession session = request.getSession();
			User u = (User) session.getAttribute("sessionUser");
			building.setUser(u);
			int status = propertyDAO.addBuilding(building);
			model.addAttribute("building", new Building());
			
			if (status > 0) {
				model.addAttribute("propertyList", propertyDAO.userBuilding(u));
				model.addAttribute("homeList", propertyDAO.userHomes(u));
				model.addAttribute("homeCount", allocatedHomesDAO.allocatedHomesCount(u));
				model.addAttribute("BuildingCount", propertyDAO.noOfBuildings(u));
				model.addAttribute("message", "success");
				return "home";
			} else {
				return "error";
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "error";
	}

	// Method rendering Add Home Page

	@RequestMapping(value = "addHome", method = RequestMethod.GET)
	public String addHomePage(Locale locale, Model model, HttpServletRequest request) {
		// added this now to populate building List
		try {
			HttpSession session = request.getSession();
			User u = (User) session.getAttribute("sessionUser");
			model.addAttribute("buildingList", propertyDAO.retrieveMyBuildingList(u));
			model.addAttribute("home", new Home());
			return "addHome";
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "error";
	}

	// Method saving new Home to DB
	@RequestMapping(value = "saveHome", method = RequestMethod.POST)
	public String saveHome(Locale locale, Model model, Home home, HttpServletRequest request) {
		try {
			HttpSession session = request.getSession();
			User u = (User) session.getAttribute("sessionUser");
			int status = propertyDAO.addHome(home);
			model.addAttribute("buildingList", propertyDAO.retrieveMyBuildingList(u));
			model.addAttribute("home", new Home());
			model.addAttribute("propertyList", propertyDAO.userBuilding(u));
			model.addAttribute("homeList", propertyDAO.userHomes(u));
			model.addAttribute("homeCount", allocatedHomesDAO.allocatedHomesCount(u));
			model.addAttribute("BuildingCount", propertyDAO.noOfBuildings(u));
			if (status > 0) {
				
				model.addAttribute("message", "success");
				return "home";
			} else {
				model.addAttribute("message", "failure");
				return "home";
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "error";
	}

	// Method to retrieve Building List for Search Building Page
	@RequestMapping(value = "searchBuilding", method = RequestMethod.GET)
	public String BuildingPageList(Locale locale, Model model, HttpServletRequest request) {
		try {
			String userName = request.getParameter("username");
			User user = userDAO.searchByUserName(userName);
			ArrayList<Building> buildingList = propertyDAO.retrieveBuildingList();
			HttpSession session = request.getSession();
			session.setAttribute("user", user);
			model.addAttribute("buildingList", buildingList);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "searchBuilding";
	}

	// Method to retrieve Apartment List
	@RequestMapping(value = "searchApartment", method = RequestMethod.GET)
	public String ApartmentPageList(Locale locale, Model model, HttpServletRequest request) {
		try {

			String buildingId = request.getParameter("buildingId");
			Building b = propertyDAO.findBuildingById(buildingId);
			ArrayList<Home> aptList = propertyDAO.availableHomeList(b);
			model.addAttribute("aptList", aptList);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "searchApartment";

	}

	// Method to send message
	@RequestMapping(value = "sendMessage", method = RequestMethod.GET)
	public String sendMessage(Locale locale, Model model, HttpServletRequest request) {
		try {

			HttpSession session = request.getSession();			
			User u = (User) session.getAttribute("sessionUser");
			User sender = (User) session.getAttribute("sessionUser");
			String s = request.getParameter("homeId");
			Home h = propertyDAO.findHomeById(Integer.parseInt(s));
			User reciever = h.getBuilding().getUser();
			Messages m = new Messages();
			m.setReciever(reciever);
			m.setSender(sender);
			m.setMessage("I would like to view this apartment");
			int status = userDAO.addMessage(m);
			ArrayList<Building> buildingList = propertyDAO.retrieveBuildingList();
			model.addAttribute("propertyList", propertyDAO.userBuilding(u));
			model.addAttribute("homeList", propertyDAO.userHomes(u));
			model.addAttribute("homeCount", allocatedHomesDAO.allocatedHomesCount(u));
			model.addAttribute("BuildingCount", propertyDAO.noOfBuildings(u));
			if (status > 0) {

				model.addAttribute("buildingList", buildingList);
				model.addAttribute("message", "success");

				return "home";
			} else {
				model.addAttribute("buildingList", buildingList);
				model.addAttribute("message", "failure");
				//
				return "home";
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "home";
	}

	@RequestMapping(value = "getMessages", method = RequestMethod.GET)
	public String retrieveMessages(Locale locale, Model model, HttpServletRequest request) {
		try {

			HttpSession session = request.getSession();
			User user = (User) session.getAttribute("sessionUser");

			// ArrayList<Messages> receivedList =
			// userDAO.retrieveInbox(user.getUserId());
			// ArrayList<Messages> sentList =
			// userDAO.retrieveSentMessages(user.getUserId());

			ArrayList<Messages> receivedList = userDAO.retrieveInbox(user);
			ArrayList<Messages> sentList = userDAO.retrieveSentMessages(user);

			model.addAttribute("receivedList", receivedList);
			model.addAttribute("sentList", sentList);

			return "messageInbox";

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "messageInbox";
	}

	@RequestMapping(value = "logout")
	public String logout(Model model, HttpServletRequest request) {
		request.getSession().invalidate();
		User user = new User();
		model.addAttribute("user", user);
		return "index";
	}

	// Method to check the availability of user name
	@RequestMapping(value = "checkifUserIdisAvailable", method = RequestMethod.POST)
	public @ResponseBody int checkUserIdAvailability(String id) {

		try {

			int availability = userDAO.checkUserIDAvailability(id);
			if (availability > 0) {
				return availability;
			} else {
				return 0;
			}

		} catch (Exception e) {
			return 0;
		}

	}

	@RequestMapping(value = "searchUser", method = RequestMethod.GET)
	public String searchUserPage(Locale locale, Model model,
			HttpServletRequest request) {
		try {
			//int userId = Integer.parseInt(request.getParameter("userId"));
			int homeId = Integer.parseInt(request.getParameter("homeId"));
			String userName = "0";
			Role r = roleDAO.findRoleBy(3);
			ArrayList<User> usersList = userDAO.searchAllUsers(userName, r);
			model.addAttribute("usersList", usersList);
			model.addAttribute("homeId", homeId);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		}
		return "searchUser";
	}
	
	@RequestMapping(value = "searchforLesee", method = RequestMethod.POST)
	public String searchUsers(Locale locale, Model model,
			HttpServletRequest request) {
		try {
			String userName = request.getParameter("userName");
			String s = request.getParameter("homeId");
			int homeId = Integer.parseInt(s);
			Role r = roleDAO.findRoleBy(3);
			ArrayList<User> usersList = userDAO.searchAllUsers(userName, r);
			model.addAttribute("usersList", usersList);
			model.addAttribute("homeId", homeId);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "searchUser";
	}
	
	@RequestMapping(value = "saveAllocation", method = RequestMethod.GET)
	public String saveAssignment(Locale locale, Model model,
			HttpServletRequest request) {
		try {
			HttpSession session = request.getSession();
			int userId = Integer.parseInt(request.getParameter("userId"));
			 User lesee = userDAO.searchUserById(userId);
			int homeId = Integer.parseInt(request.getParameter("homeId"));
			propertyDAO.updateAllocation(homeId, lesee);
			User u = (User) session.getAttribute("sessionUser");
			model.addAttribute("propertyList", propertyDAO.userBuilding(u));
			model.addAttribute("homeList", propertyDAO.userHomes(u));
			model.addAttribute("homeCount", allocatedHomesDAO.allocatedHomesCount(u));
			model.addAttribute("buildingCount", propertyDAO.buildingCount(u));
			model.addAttribute("message", "success");			
		} catch (Exception e) {
			model.addAttribute("message", "failure");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "home";
	}
}
